// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include <QApplication>
#include <QCommandLineParser>
#include <QDir>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSqlDatabase>
#include <QSqlError>
#include <QStandardPaths>
#include <QUrl>

#include "app.h"
#include "kroneconfig.h"

#include "logic/accountmodel.h"
#include "logic/databaseModels.h"

constexpr auto URI = "org.kde.krone.private";

Q_DECL_EXPORT int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("krone");

    KAboutData aboutData(
        // The program name used internally.
        QStringLiteral("krone"),
        // A displayable program name string.
        i18nc("@title", "Krone"),
        // The program version string.
        QStringLiteral("0.1"),
        // Short description of what the app does.
        i18nc("@title", "A money tracker application"),
        // The license this code is released under.
        KAboutLicense::GPL_V3,
        // Copyright Statement.
        i18n("(c) 2023"));
    aboutData.addAuthor(i18nc("@info:credit", "Louis Schul"), i18nc("@info:credit", "Maintainer and creator"), QStringLiteral("schul9louis@gmail.com"));
    //, QStringLiteral("https://yourwebsite.com"));
    aboutData.setTranslator(i18nc("NAME OF TRANSLATORS", "Your names"), i18nc("EMAIL OF TRANSLATORS", "Your emails"));

    KAboutData::setApplicationData(aboutData);
    QGuiApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("org.kde.krone")));

    QQmlApplicationEngine engine;

    auto config = kroneConfig::self();

    qmlRegisterSingletonInstance("org.kde.krone.private", 1, 0, "Config", config);

    qmlRegisterSingletonInstance<DatabaseModels>(URI, 1, 0, "DatabaseModels", &DatabaseModels::instance());

    qmlRegisterSingletonType("org.kde.krone.private", 1, 0, "About", [](QQmlEngine *engine, QJSEngine *) -> QJSValue {
        return engine->toScriptValue(KAboutData::applicationData());
    });

    App application;
    qmlRegisterSingletonInstance("org.kde.krone.private", 1, 0, "App", &application);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
