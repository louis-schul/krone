
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

#include "projectsmodel.h"
#include "databaseModels.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include <QCoroFuture>
#include <QCoroTask>

#include <klocalizedstring.h>

ProjectsModel::ProjectsModel(DatabaseModels *models)
    : QAbstractListModel(models)
    , m_models(models)
{
    QFuture<std::vector<Project>> future = models->database().getResults<Project>(QStringLiteral("SELECT * FROM Projects"));

    QCoro::connect(std::move(future), this, [this](std::vector<Project> &&projects) {
        beginResetModel();
        m_projects = projects;
        endResetModel();

        for (Project entry : m_projects) {
            if (!entry.achieved)
                m_projectsName.insert(entry.name);
        }
    });
}

QVariant ProjectsModel::data(const QModelIndex &index, int role) const
{
    const auto &entry = m_projects.at(index.row());
    switch (role) {
    case ProjectRoles::Id:
        return entry.id;
    case ProjectRoles::Name:
        return entry.name;
    case ProjectRoles::GoalAmount:
        return entry.goalAmount;
    case ProjectRoles::CurrentAmount:
        return entry.currentAmount;
    case ProjectRoles::Achieved:
        return entry.achieved;
    case ProjectRoles::Color:
        return entry.color;
    case ProjectRoles::Percent:
        return (float)entry.currentAmount / (float)entry.goalAmount;
    }

    return {};
}

int ProjectsModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_projects.size();
}

QHash<int, QByteArray> ProjectsModel::roleNames() const
{
    return {{ProjectRoles::Id, "id"},
            {ProjectRoles::Name, "projectName"},
            {ProjectRoles::GoalAmount, "goalAmount"},
            {ProjectRoles::CurrentAmount, "currentAmount"},
            {ProjectRoles::Achieved, "achieved"},
            {ProjectRoles::Color, "projectColor"},
            {ProjectRoles::Percent, "percentage"}};
}

bool ProjectsModel::addProject(const QString &name, const int goalAmount, const QColor &color)
{
    if (m_projectsName.contains(name))
        return false;

    QString query = QStringLiteral("INSERT INTO Projects (Name, GoalAmount, Color) VALUES (?, ?, ?) RETURNING ID");
    auto futureID = m_models->database().getResult<SingleValue<int>>(query, name, goalAmount, color);

    QCoro::connect(std::move(futureID), this, [this, name, goalAmount, color](std::optional<SingleValue<int>> &&futureID) {
        auto id = futureID.value().value;

        beginInsertRows({}, m_projects.size(), m_projects.size());
        m_projects.push_back(Project{.id = id, .name = name, .goalAmount = goalAmount, .currentAmount = 0, .achieved = false, .color = color});
        endInsertRows();
    });

    m_projectsName.insert(name);

    return true;
}

bool ProjectsModel::updateProject(int index, const QString &name, const int goalAmount, const QColor &color)
{
    Project *entry = &m_projects[index];
    bool keepName = entry->name == name;
    if (!keepName && m_projectsName.contains(name))
        return false;

    QString query = QStringLiteral("UPDATE Projects SET Name = ?, GoalAmount = ?, Color = ?, WHERE ID = ?");

    m_models->database().execute(query, name, goalAmount, color, entry->id);

    entry->name = name;
    entry->goalAmount = goalAmount;
    entry->color = color;

    QModelIndex entryIndex = createIndex(index, 0, entry);
    Q_EMIT dataChanged(entryIndex, entryIndex);

    return true;
}

void ProjectsModel::removeProject(int index)
{
    if (index < 0)
        return;

    Project *entry = &m_projects[index];
    int id = entry->id;

    m_models->database().execute(QStringLiteral("DELETE FROM Projects WHERE ID = ?"), id);

    beginRemoveRows({}, index, index);
    m_projects.erase(m_projects.begin() + index);
    endRemoveRows();

    m_models->accountModel()->resetToDefaultProject(id);
}

void ProjectsModel::addMoney(const int amount, int index, const int id)
{
    Project *entry = nullptr;
    int entryId;
    if (id != -1) {
        entryId = id;
    } else if (index != -1) {
        entry = &m_projects[index];
        entryId = entry->id;
    } else {
        return;
    }

    QString query = QStringLiteral("UPDATE Projects SET CurrentAmount = CurrentAmount + ? WHERE ID = ?");
    m_models->database().execute(query, amount, entryId);

    if (entry) {
        entry->currentAmount += amount;

        QModelIndex entryIndex = createIndex(index, 0, entry);
        Q_EMIT dataChanged(entryIndex, entryIndex);
    }
}
