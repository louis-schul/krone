// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

#include "groupinfo.h"
#include "databaseModels.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include <QCoroFuture>
#include <QCoroTask>

#include <klocalizedstring.h>

GroupInfo::GroupInfo(DatabaseModels *models)
    : QAbstractListModel(models)
    , m_models(models)
{
    QFuture<std::vector<Info>> future = models->database().getResults<Info>(QStringLiteral("SELECT * FROM GroupInfo"));

    QString defaultName = i18nc("@defaultName:transactionGroup", "Default");
    QColor defaultColor = QColor("#3daee9");
    QCoro::connect(std::move(future), this, [this, defaultName, defaultColor](std::vector<Info> &&infos) {
        if (infos.empty()) {
            // TODO ?? Use system accent Color ???
            addGroup(defaultName, defaultColor);
        } else {
            beginResetModel();
            m_informations = infos;
            endResetModel();
        }
        for (Info entry : m_informations) {
            m_colorsByNames[entry.name] = entry.color;
        }
    });
}

QVariant GroupInfo::data(const QModelIndex &index, int role) const
{
    const auto &entry = m_informations.at(index.row());
    switch (role) {
    case TransactionRoles::Id:
        return entry.id;
    case TransactionRoles::Name:
        return entry.name;
    case TransactionRoles::Color:
        return entry.color;
    }

    return {};
}

int GroupInfo::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_informations.size();
}

QHash<int, QByteArray> GroupInfo::roleNames() const
{
    return {{TransactionRoles::Id, "id"}, {TransactionRoles::Name, "name"}, {TransactionRoles::Color, "color"}};
}

QColor GroupInfo::getColor(const QString &groupName) const
{
    return m_colorsByNames.contains(groupName) ? m_colorsByNames[groupName] : QColorConstants::Transparent;
}

bool GroupInfo::addGroup(const QString &name, const QColor &color)
{
    if (m_colorsByNames.contains(name))
        return false;

    QString query = QStringLiteral("INSERT INTO GroupInfo (Name, Color) VALUES (?, ?) RETURNING ID");
    auto futureID = m_models->database().getResult<SingleValue<int>>(query, name, color);

    QCoro::connect(std::move(futureID), this, [this, name, color](std::optional<SingleValue<int>> &&futureID) {
        auto id = futureID.value().value;

        beginInsertRows({}, m_informations.size(), m_informations.size());
        m_informations.push_back(Info{.id = id, .name = name, .color = color});
        endInsertRows();
    });

    m_colorsByNames[name] = color;

    return true;
}

bool GroupInfo::updateGroup(int index, const QString &name, const QColor &color)
{
    Info *entry = &m_informations[index];
    bool keepName = entry->name == name;
    if (!keepName && m_colorsByNames.contains(name))
        return false;

    QString query = QStringLiteral("UPDATE GroupInfo SET Name = ?, Color = ? WHERE ID = ?");

    m_models->database().execute(query, name, color, entry->id);

    entry->name = name;
    entry->color = color;

    Q_EMIT dataChanged(createIndex(index, 0, entry), createIndex(index, 0, entry));

    return true;
}

void GroupInfo::removeGroup(int index)
{
    Info *entry = &m_informations[index];
    int id = entry->id;
    if (id == 1)
        return;

    m_models->database().execute(QStringLiteral("DELETE FROM GroupInfo WHERE ID = ?"), id);

    beginRemoveRows({}, index, index);
    m_informations.erase(m_informations.begin() + index);
    endRemoveRows();

    QString name = entry->name;
    m_models->accountModel()->resetToDefaultGroup(name);
}
