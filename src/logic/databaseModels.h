// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

// BASED ON : https://invent.kde.org/multimedia/audiotube/-/blob/master/src/library.h
// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QAbstractListModel>
#include <QObject>
#include <QSortFilterProxyModel>

#include <ThreadedDatabase>

#include <memory>

#include "accountmodel.h"
#include "groupinfo.h"
#include "projectsmodel.h"

class DatabaseModels : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractListModel *accountModel READ accountModel CONSTANT)
    Q_PROPERTY(QAbstractListModel *groupInfo READ groupInfo CONSTANT)
    Q_PROPERTY(QAbstractListModel *projectsModel READ projectsModel CONSTANT)

public:
    explicit DatabaseModels(QObject *parent = nullptr);
    ~DatabaseModels();

    static DatabaseModels &instance();

    GroupInfo *groupInfo();
    AccountModel *accountModel();
    ProjectsModel *projectsModel();
    ThreadedDatabase &database();

private:
    GroupInfo *m_groupInfo;
    AccountModel *m_accountModel;
    ProjectsModel *m_projectsModel;
    std::unique_ptr<ThreadedDatabase> m_database;
};
