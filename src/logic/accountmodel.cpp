// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

#include "accountmodel.h"
#include "databaseModels.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include <QStringBuilder>

#include <QCoroFuture>
#include <QCoroTask>

#include <klocalizedstring.h>

AccountModel::AccountModel(DatabaseModels *models)
    : QAbstractListModel(models)
    , m_models(models)
{ // Will be init by the MainPage filter comboBox
    QString query = QStringLiteral("SELECT * FROM Total ");
    QFuture<std::optional<Total>> future = m_models->database().getResult<Total>(query);

    Total defaultTotal{.id = -1, .earned = 0, .loss = 0};

    QCoro::connect(std::move(future), this, [this, defaultTotal](std::optional<Total> &&totals) {
        Total test = totals.value_or(defaultTotal);
        if (test.id == -1) {
            test.id = 0;
            m_models->database().execute(QStringLiteral("INSERT INTO Total (Earned, Lost) VALUES (0, 0)"));
        }
        m_totals = test;
        // qDebug() << totals.value_or(defaultTotal).id;
    });
}

void AccountModel::initModel()
{
    QDate current = QDate::currentDate();

    QString quote = QStringLiteral("'");

    QString today = quote % current.toString(Qt::ISODate) % quote;
    QString startMonth = quote % QDate(current.year(), current.month(), 1).toString(Qt::ISODate) % quote;
    QString endMonth = quote % QDate(current.year(), current.month(), current.daysInMonth()).toString(Qt::ISODate) % quote;

    QString startYear = quote % QDate(current.year(), 1, 1).toString(Qt::ISODate) % quote;
    QString endYear = quote % QDate(current.year(), 12, 31).toString(Qt::ISODate) % quote;

    QString base = QStringLiteral("WHERE Date ");
    QString equal = QStringLiteral("= ");
    QString between = QStringLiteral("BETWEEN ");
    QString and = QStringLiteral(" AND ");

    QString queryAddon = QStringLiteral("");
    switch (m_currentDisplay) {
    case DisplayType::Today:
        queryAddon = base % equal % today;
        break;
    case DisplayType::Month:
        queryAddon = base % between % startMonth % and % endMonth;
        break;
    case DisplayType::Year:
        queryAddon = base % between % startYear % and % endYear;
        break;
    default:
        break;
    }
    QString query = QStringLiteral("SELECT * FROM Account ") % queryAddon;

    QFuture<std::vector<Transaction>> future = m_models->database().getResults<Transaction>(query);

    QCoro::connect(std::move(future), this, [this](std::vector<Transaction> &&transactions) {
        beginResetModel();
        m_account = transactions;
        endResetModel();
    });
}

QVariant AccountModel::data(const QModelIndex &index, int role) const
{
    const auto &entry = m_account.at(index.row());
    switch (role) {
    case TransactionRoles::Id:
        return entry.id;
    case TransactionRoles::Amount:
        return entry.amount;
    case TransactionRoles::Title:
        return entry.title;
    case TransactionRoles::Description:
        return entry.description;
    case TransactionRoles::Date:
        return entry.date;
    case TransactionRoles::Group:
        return entry.group;
    case TransactionRoles::GroupColor:
        return m_models->groupInfo()->getColor(entry.group);
    case TransactionRoles::Currency:
        return m_currencies[entry.currency];
    case TransactionRoles::Type:
        return entry.amount >= 0 ? QStringLiteral("assets") : QStringLiteral("expenses");
    }

    return {};
}

int AccountModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_account.size();
}

QHash<int, QByteArray> AccountModel::roleNames() const
{
    return {{TransactionRoles::Id, "id"},
            {TransactionRoles::Amount, "amount"},
            {TransactionRoles::Title, "title"},
            {TransactionRoles::Description, "description"},
            {TransactionRoles::Date, "date"},
            {TransactionRoles::Group, "group"},
            {TransactionRoles::GroupColor, "groupColor"},
            {TransactionRoles::Currency, "currency"},
            {TransactionRoles::Type, "type"}};
}

void AccountModel::addTransaction(const int amount,
                                  const QString &title,
                                  const QString &description,
                                  const QString &date,
                                  const QString &group,
                                  const QString &currency,
                                  const int projectID)
{
    updateForeignTable(amount, projectID);

    QString isoDate = convertToISODate(date);

    QString query =
        QStringLiteral("INSERT INTO Account (Amount, Title, Description, Date, GroupName, Currency, ProjectID) VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING ID");
    auto futureID = m_models->database().getResult<SingleValue<int>>(query, amount, title, description, isoDate, group, currency, projectID);

    if (canAddToModel(date)) {
        QCoro::connect(std::move(futureID),
                       this,
                       [this, amount, title, description, isoDate, group, currency, projectID](std::optional<SingleValue<int>> &&futureID) {
                           auto id = futureID.value().value;

                           beginInsertRows({}, m_account.size(), m_account.size());
                           m_account.push_back(Transaction{.id = id,
                                                           .amount = amount,
                                                           .title = title,
                                                           .description = description,
                                                           .date = isoDate,
                                                           .group = group,
                                                           .currency = currency,
                                                           .projectID = projectID});
                           endInsertRows();
                       });
    }
}

void AccountModel::removeTransaction(int index)
{
    updateForeignTable(m_account[index].amount, m_account[index].projectID, true);

    m_models->database().execute(QStringLiteral("DELETE FROM Account WHERE ID = ?"), m_account[index].id);

    beginRemoveRows({}, index, index);
    m_account.erase(m_account.begin() + index);
    endRemoveRows();
}

void AccountModel::updateTransaction(int index,
                                     const int amount,
                                     const QString &title,
                                     const QString &description,
                                     const QString &date,
                                     const QString &group,
                                     const QString &currency)
{
    QString isoDate = convertToISODate(date);

    QString query = QStringLiteral("UPDATE Account SET Amount = ?, Title = ?, Description = ?, Date = ?, GroupName = ?, Currency = ? WHERE ID = ?");

    Transaction *entry = &m_account[index];
    m_models->database().execute(query, amount, title, description, isoDate, group, currency, entry->id);

    // remove the old amount
    updateForeignTable(entry->amount, entry->id, true);
    // add the new amount
    updateForeignTable(amount, entry->id);

    entry->amount = amount;
    entry->title = title;
    entry->description = description;
    entry->date = isoDate;
    entry->group = group;
    entry->currency = currency;

    Q_EMIT dataChanged(createIndex(index, 0, entry), createIndex(index, 0, entry));
}

void AccountModel::resetToDefaultGroup(const QString &oldGroupName)
{
    QString defaultName = i18nc("@defaultName:transactionGroup", "Default");
    for (int i = 0; i < int(m_account.size()); i++) {
        auto *entry = &m_account[i];
        if (entry->group == oldGroupName) {
            entry->group = defaultName;

            Q_EMIT dataChanged(createIndex(i, 0, entry), createIndex(i, 0, entry));
        }
    }

    QString query = QStringLiteral("UPDATE Account SET GroupName = ? WHERE GroupName = ?");
    m_models->database().execute(query, defaultName, oldGroupName);
}

void AccountModel::resetToDefaultProject(const int projectID)
{
    QString query = QStringLiteral("UPDATE Account SET ProjectID = -1 WHERE ProjectID = ?");
    m_models->database().execute(query, projectID);
}

void AccountModel::changeDisplayType(const QString &displayTypeName)
{
    if (displayTypeName == i18nc("@name:filter", "Month")) {
        m_currentDisplay = DisplayType::Month;
    } else if (displayTypeName == i18nc("@name:filter", "Year")) {
        m_currentDisplay = DisplayType::Year;
    } else if (displayTypeName == i18nc("@name:filter", "Forever")) {
        m_currentDisplay = DisplayType::Ever;
    } else {
        m_currentDisplay = DisplayType::Today;
    }

    initModel();
}

QDate AccountModel::convertToDate(const QString &date)
{
    auto info = date.split(QStringLiteral("/"));

    int year = info[2].toInt();
    int month = info[1].toInt();
    int day = info[0].toInt();

    return QDate(year, month, day);
}

QString AccountModel::convertToISODate(const QString &date)
{
    return convertToDate(date).toString(Qt::ISODate);
}

bool AccountModel::canAddToModel(const QString &date)
{
    QDate givenDate = convertToDate(date);
    QDate today = QDate::currentDate();
    switch (m_currentDisplay) {
    case DisplayType::Today:
        return givenDate == today;
    case DisplayType::Month:
        return givenDate.month() == today.month();
    case DisplayType::Year:
        return givenDate.year() == today.year();
    default:
        return true;
    }
}

void AccountModel::updateForeignTable(const int amount, const int projectID, const bool deleted)
{
    int finalAmount = deleted ? -amount : amount;
    m_models->projectsModel()->addMoney(finalAmount, -1, projectID);

    int total;
    QString addons;
    if (amount < 0) {
        m_totals.loss += finalAmount;
        total = m_totals.loss;
        addons = QStringLiteral("Lost = ");
    } else {
        m_totals.earned += finalAmount;
        total = m_totals.earned;
        addons = QStringLiteral("Earned = ");
    }
    QString query = QStringLiteral("UPDATE Total SET ") % addons % QString::number(total);

    m_models->database().execute(query);

    Q_EMIT totalChanged();
}
