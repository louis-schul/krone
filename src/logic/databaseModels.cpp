﻿// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

// BASED ON : https://invent.kde.org/multimedia/audiotube/-/blob/master/src/library.cpp
// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#include "databaseModels.h"
#include "accountmodel.h"

#include <QDir>
#include <QGuiApplication>
#include <QStandardPaths>
#include <QStringBuilder>

#include <QCoroFuture>
#include <QCoroTask>
#include <ThreadedDatabase>

DatabaseModels::DatabaseModels(QObject *parent)
    : QObject{parent}
    , m_database(ThreadedDatabase::establishConnection([]() -> DatabaseConfiguration {
        const auto databaseDirectory = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        // Make sure the database directory exists
        QDir(databaseDirectory).mkpath(QStringLiteral("."));

        DatabaseConfiguration config;
        config.setType(DatabaseType::SQLite);
        config.setDatabaseName(
            QDir::cleanPath(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) % QDir::separator() % qGuiApp->applicationName()));
        config.setType(DatabaseType::SQLite);
        return config;
    }()))
{
    m_database->runMigrations(QStringLiteral(":/contents/migrations/"));
    m_groupInfo = new GroupInfo(this);
    m_accountModel = new AccountModel(this);
    m_projectsModel = new ProjectsModel(this);
}

DatabaseModels::~DatabaseModels() = default;

DatabaseModels &DatabaseModels::instance()
{
    static DatabaseModels inst;
    return inst;
}

GroupInfo *DatabaseModels::groupInfo()
{
    return m_groupInfo;
}

AccountModel *DatabaseModels::accountModel()
{
    return m_accountModel;
}

ProjectsModel *DatabaseModels::projectsModel()
{
    return m_projectsModel;
}

ThreadedDatabase &DatabaseModels::database()
{
    return *m_database;
}
