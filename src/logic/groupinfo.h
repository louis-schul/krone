// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QColor>
#include <QSqlDatabase>
#include <QSqlTableModel>

#include <memory>

class DatabaseModels;

struct Info {
    using ColumnTypes = std::tuple<int, QString, QColor>;

    int id;
    QString name;
    QColor color;
};

class GroupInfo : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int length READ length CONSTANT)

public:
    enum TransactionRoles { Id = Qt::UserRole + 1, Name, Color };

public:
    GroupInfo(DatabaseModels *models);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int length()
    {
        return m_informations.size();
    };

    Q_INVOKABLE QColor getColor(const QString &groupName) const;
    Q_INVOKABLE bool addGroup(const QString &name, const QColor &color);
    Q_INVOKABLE bool updateGroup(int index, const QString &name, const QColor &color);
    Q_INVOKABLE void removeGroup(int index);

private:
    DatabaseModels *m_models;
    std::vector<Info> m_informations;
    QMap<QString, QColor> m_colorsByNames;
};
