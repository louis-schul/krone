// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QDate>
#include <QHash>
#include <QSqlDatabase>
#include <QSqlTableModel>

#include <memory>

class DatabaseModels;

struct Total {
    using ColumnTypes = std::tuple<int, int, int>;

    int id;

    int earned;
    int loss;
};

struct Transaction {
    using ColumnTypes = std::tuple<int, int, QString, QString, QString, QString, QString, int>;

    int id;
    // Should be devided by 100
    int amount;
    QString title;
    QString description;
    QString date;
    QString group;
    QString currency;
    int projectID;
};

class AccountModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString displayType WRITE changeDisplayType)
    Q_PROPERTY(QStringList currencies READ getCurrencies)
    Q_PROPERTY(int totalLoss READ getLoss NOTIFY totalChanged)
    Q_PROPERTY(int totalEarned READ getEarned NOTIFY totalChanged)

public:
    enum TransactionRoles { Id = Qt::UserRole + 1, Title, Amount, Description, Date, Group, GroupColor, Currency, Type };

    enum DisplayType { Today = Qt::UserRole + 1, Month, Year, Ever };

public:
    AccountModel(DatabaseModels *models);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    Q_INVOKABLE void removeTransaction(int index);
    Q_INVOKABLE void addTransaction(const int amount,
                                    const QString &title,
                                    const QString &description,
                                    const QString &date,
                                    const QString &group,
                                    const QString &currency,
                                    const int projectID = -1);
    Q_INVOKABLE void updateTransaction(int index,
                                       const int amount,
                                       const QString &title,
                                       const QString &description,
                                       const QString &date,
                                       const QString &group,
                                       const QString &currency);
    void initModel();

    void resetToDefaultGroup(const QString &oldGroupName);
    void resetToDefaultProject(const int projectID);

    void changeDisplayType(const QString &displayTypeName);
    QStringList getCurrencies()
    {
        return m_currencies.values();
    };
    int getLoss()
    {
        return m_totals.loss;
    };
    int getEarned()
    {
        return m_totals.earned;
    };

Q_SIGNALS:
    void totalChanged();

private:
    QDate convertToDate(const QString &date);
    QString convertToISODate(const QString &date);
    bool canAddToModel(const QString &date);
    void updateForeignTable(const int amount, const int projectID, const bool deleted = false);

    DisplayType m_currentDisplay;

    DatabaseModels *m_models;
    std::vector<Transaction> m_account;
    Total m_totals;

    // TODO: get system settings currency list
    QHash<QString, QString> m_currencies{{QStringLiteral("EUR"), QStringLiteral("€")}, {QStringLiteral("USD"), QStringLiteral("$")}};
};
