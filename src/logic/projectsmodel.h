// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QColor>
#include <QSet>
#include <QSqlDatabase>
#include <QSqlTableModel>

#include <memory>

class DatabaseModels;

struct Project {
    using ColumnTypes = std::tuple<int, QString, int, int, bool, QColor>;

    int id;
    QString name;
    int goalAmount;
    int currentAmount;
    bool achieved;
    QColor color;
};

class ProjectsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ProjectRoles { Id = Qt::UserRole + 1, Name, GoalAmount, CurrentAmount, Achieved, Color, Percent };

public:
    ProjectsModel(DatabaseModels *models);

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    Q_INVOKABLE bool addProject(const QString &name, const int goalAmount, const QColor &color);
    Q_INVOKABLE bool updateProject(int index, const QString &name, const int goalAmount, const QColor &color);
    Q_INVOKABLE void removeProject(int index);
    Q_INVOKABLE void addMoney(const int amount, int index = -1, const int id = -1);

private:
    DatabaseModels *m_models;
    std::vector<Project> m_projects;

    QSet<QString> m_projectsName;
};
