// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard
import org.kde.kirigamiaddons.components 1.0 as Components
import org.kde.kitemmodels 1.0

import org.kde.krone.private 1.0

import "qrc:/contents/ui/components"
import "qrc:/contents/ui/dialogs"
import "qrc:/contents/ui/projects"

GridLayout {
    id: root

    required property int availableWidth
    required property int availableHeight
    property alias deleteWarningDialog: cardsView.deleteWarningDialog

    flow: applicationWindow().isWideScreen ? GridLayout.LeftToRight : GridLayout.TopToBottom

    rowSpacing: Kirigami.Units.largeSpacing
    columnSpacing: applicationWindow().isWideScreen ? Kirigami.Units.largeSpacing : 0

    ColumnLayout {
        id: firstColumn

        spacing: applicationWindow().isWideScreen ? Kirigami.Units.largeSpacing : 0

        Layout.alignment: Qt.AlignTop
        Layout.preferredWidth: applicationWindow().isWideScreen ? Kirigami.Units.gridUnit * 30 : availableWidth

        FormCard.FormCard {
            id: totalCard

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            ColumnLayout {
                Layout.fillWidth: true
                Layout.margins: Kirigami.Units.largeSpacing
                RowLayout {
                    id: topPart

                    Kirigami.Heading {
                        id: total

                        text: i18n("Total")+":"
                        level: 1
                    }
                    Kirigami.Heading {
                        Layout.fillWidth: true

                        text: DatabaseModels.accountModel.totalEarned/100 + DatabaseModels.accountModel.totalLoss/100
                        level: 1
                        horizontalAlignment: Text.AlignRight
                    }
                }

                Kirigami.Separator {
                    Layout.fillWidth: true
                }

                RowLayout {
                    id: bottomPart

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    spacing: Kirigami.Units.largeSpacing

                    // Seems like a useless layout but this assure us to have the exact same width for each
                    RowLayout {
                        Layout.preferredWidth: totalCard.width / 2
                        spacing: 0
                        Kirigami.Heading {
                            text: i18n("Income")+":"
                            level: 3
                        }

                        Kirigami.Heading {
                            Layout.fillWidth: true

                            text: DatabaseModels.accountModel.totalEarned/100
                            level: 2
                            color: "#38B87C"
                            horizontalAlignment: Text.AlignRight
                        }
                    }

                    Kirigami.Separator {
                        Layout.preferredHeight: bottomPart.height
                    }

                    RowLayout {
                        Layout.preferredWidth: totalCard.width / 2
                        spacing: 0
                        Kirigami.Heading {
                            text: i18n("Expense")+":"
                            level: 3
                        }

                        Kirigami.Heading {
                            Layout.fillWidth: true

                            text: DatabaseModels.accountModel.totalLoss/100
                            level: 2
                            color: "#F34541"
                            horizontalAlignment: Text.AlignRight
                        }
                    }
                }
            }
        }

        Loader {
            id: projectLoader

            active: applicationWindow().isWideScreen
            sourceComponent: ProjectView { anchors.fill: parent }

            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: {
                const spacing = Kirigami.Units.largeSpacing
                const finalSize = root.availableHeight - spacing - totalCard.height

                return applicationWindow().isWideScreen ? finalSize : 0
            }
            Layout.alignment: Qt.AlignTop
        }
    }

    ColumnLayout {
        id: secondColumn

        spacing: Kirigami.Units.largeSpacing

        Layout.preferredWidth: {
            const finalSize = availableWidth - firstColumn.width - root.columnSpacing
            return applicationWindow().isWideScreen ? finalSize : availableWidth
        }
        Layout.alignment: Qt.AlignTop

        FormCard.FormCard {
            id: chartCard

            maximumWidth: applicationWindow().isWideScreen ? secondColumn.width : Kirigami.Units.gridUnit * 30

            Layout.fillWidth: true
            Layout.preferredHeight: Kirigami.Units.gridUnit * 15

            FormCard.FormHeader {
                Layout.fillWidth: true
                title: i18n("General view")
            }

            // TODO: Figure out how Quick Charts works
            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                Kirigami.Heading {

                    text: "CHART GOES HERE"

                    anchors {
                        verticalCenter: parent.verticalCenter
                        horizontalCenter: parent.horizontalCenter
                    }
                    level: 1
                }
            }
        }

        FormCard.FormCard {
            maximumWidth: applicationWindow().isWideScreen ? secondColumn.width : Kirigami.Units.gridUnit * 30

            // Hack to make a "fillHeight" with a minimumSize (fillHeight doesn't work)
            Layout.preferredHeight: {
                const spacing = Kirigami.Units.largeSpacing * (applicationWindow().isWideScreen ? 1 : 2)
                const otherCardHeight = chartCard.height + totalCard.height * (applicationWindow().isWideScreen ? 0 : 1)

                const finalSize = root.availableHeight - spacing - otherCardHeight
                const minimumSize = Kirigami.Units.gridUnit * 18 // We can put 2 transaction on top of each other

                return finalSize > minimumSize ? finalSize : minimumSize
            }

            FormCard.FormComboBoxDelegate {
                text: i18n("Transactions")
                model: [
                    i18nc("@name:filter", "Day"),
                    i18nc("@name:filter", "Month"),
                    i18nc("@name:filter", "Year"),
                    i18nc("@name:filter", "Forever")
                ]
                currentIndex: 1
                // currentText or currentValue doesn't work :/
                onCurrentIndexChanged: DatabaseModels.accountModel.displayType = model[currentIndex]
            }

            Kirigami.CardsListView {
                id: cardsView

                property var deleteWarningDialog

                KSortFilterProxyModel {
                    id: searchFilterProxyModel
                    sourceModel: DatabaseModels.accountModel
                    filterRole: "type"
                    sortRole: "date"
                    sortOrder: Qt.DescendingOrder
                }

                clip: true
                model: searchFilterProxyModel

                delegate: TransactionDelegate{}

                Layout.fillWidth: true
                Layout.fillHeight: true

                Components.FloatingButton {
                    action: Kirigami.Action {
                        text: "Add new item"

                        icon.name: "list-add"

                        onTriggered: applicationWindow().switchToPage("TransactionCreation")
                    }
                    anchors {
                        right: parent.right
                        rightMargin: Kirigami.Units.largeSpacing
                        bottom: parent.bottom
                        bottomMargin: Kirigami.Units.largeSpacing
                    }
                }
            }
        }
    }
}
