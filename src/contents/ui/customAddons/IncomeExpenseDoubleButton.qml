// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

// LOOSELY BASED ON Kirigamiaddons DoubleFloatingButton
// SPDX-FileCopyrightText: 2023 Mathis Brüchert <mbb@kaidan.im>
// SPDX-FileCopyrightText: 2023 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0

Item {
    id: root

    required property string leftIcon
    required property string rightIcon

    property bool positive: true

    ColumnLayout {
        spacing: 0
        anchors.fill: parent

        AbstractFormDelegate {
            id: upperHolder

            Layout.fillWidth: true
            Layout.fillHeight: true

            verticalPadding: 0
            horizontalPadding: 0
            leftPadding: Kirigami.Units.largeSpacing
            rightPadding: Kirigami.Units.largeSpacing * 2 + Kirigami.Units.smallSpacing
            topPadding: Kirigami.Units.largeSpacing

            contentItem: QQC2.Button {
                id: upperButton

                property color greenColor: "#38B87C"
                background: Kirigami.ShadowedRectangle {
                    Kirigami.Theme.inherit: false
                    Kirigami.Theme.colorSet: Kirigami.Theme.Window

                    corners {
                        topLeftRadius: Kirigami.Units.largeSpacing
                        topRightRadius:Kirigami.Units.largeSpacing
                    }

                    border {
                        width: 1
                        color: {
                            let alpha = 0.2
                            let finalColor = Kirigami.Theme.textColor

                            if (root.positive || upperButton.hovered || upperHolder.hovered) {
                                alpha = 0.8
                                finalColor = upperButton.greenColor
                            } else if (upperButton.down || upperButton.visualFocus || upperHolder.down || upperHolder.visualFocus) {
                                alpha = 0.6
                                finalColor = upperButton.greenColor
                            }


                            return Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, finalColor, alpha)
                        }
                    }

                    color: if (root.positive || upperButton.hovered || upperHolder.hovered) {
                        Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, upperButton.greenColor, 0.8)
                    } else if (upperButton.down || upperButton.visualFocus || upperHolder.down || upperHolder.visualFocus) {
                        Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, upperButton.greenColor, 0.6)
                    } else {
                        Kirigami.Theme.backgroundColor
                    }

                    Behavior on color {
                        enabled: true
                        ColorAnimation {
                            duration: Kirigami.Units.longDuration
                            easing.type: Easing.OutCubic
                        }
                    }

                    Behavior on border.color {
                        enabled: true
                        ColorAnimation {
                            duration: Kirigami.Units.longDuration
                            easing.type: Easing.OutCubic
                        }
                    }
                }

                contentItem: Text {
                    text: i18n("Add")

                    font.bold: true

                    elide: Text.ElideRight
                    color: Kirigami.Theme.textColor
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                display: QQC2.AbstractButton.IconOnly

                onClicked: root.positive = true
            }

            onPressed: root.positive = true
            Accessible.onPressAction: root.positive = true
        }

        AbstractFormDelegate {
            id: lowerHolder

            Layout.fillWidth: true
            Layout.fillHeight: true

            horizontalPadding: 0
            verticalPadding: 0
            leftPadding: Kirigami.Units.largeSpacing
            rightPadding: Kirigami.Units.largeSpacing * 2 + Kirigami.Units.smallSpacing
            bottomPadding: Kirigami.Units.largeSpacing

            contentItem: QQC2.Button {
                id: lowerButton

                property color redColor: "#F34541"
                background: Kirigami.ShadowedRectangle {
                    Kirigami.Theme.inherit: false
                    Kirigami.Theme.colorSet: Kirigami.Theme.Window

                    corners {
                        bottomLeftRadius: Kirigami.Units.largeSpacing
                        bottomRightRadius: Kirigami.Units.largeSpacing
                    }

                    border {
                        width: 1
                        color: {
                            let alpha = 0.2
                            let finalColor = Kirigami.Theme.textColor

                            if (!root.positive || lowerButton.hovered || lowerHolder.hovered) {
                                alpha = 0.8
                                finalColor = lowerButton.redColor
                            } else if (lowerButton.down || lowerButton.visualFocus || lowerHolder.down || lowerHolder.visualFocus) {
                                alpha = 0.6
                                finalColor = lowerButton.redColor
                            }

                            return Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, finalColor, alpha)
                        }
                    }

                    color: if (!root.positive || lowerButton.hovered || lowerHolder.hovered) {
                        Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, lowerButton.redColor, 0.8)
                    } else if (lowerButton.down || lowerButton.visualFocus || lowerHolder.down || lowerHolder.visualFocus) {
                        Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, lowerButton.redColor, 0.6)
                    } else {
                        Kirigami.Theme.backgroundColor
                    }

                    Behavior on color {
                        enabled: true
                        ColorAnimation {
                            duration: Kirigami.Units.longDuration
                            easing.type: Easing.OutCubic
                        }
                    }

                    Behavior on border.color {
                        enabled: true
                        ColorAnimation {
                            duration: Kirigami.Units.longDuration
                            easing.type: Easing.OutCubic
                        }
                    }
                }

                contentItem: Text {
                    text: i18n("Remove")

                    font.bold: true

                    elide: Text.ElideRight
                    color: Kirigami.Theme.textColor
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                display: QQC2.AbstractButton.IconOnly

                onClicked: root.positive = false
            }

            onPressed: root.positive = false
            Accessible.onPressAction: root.positive = false
        }
    }
}
