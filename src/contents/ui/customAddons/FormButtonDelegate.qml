// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

// SLIGHTLY MODIFIED VERSION OF FormButtonDelegate, all credit goes to:
/*
 * Copyright 2022 Devin Lin <devin@kde.org>
 * SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0

/**
 * @brief A Form delegate that corresponds to a clickable button.
 *
 * Use the inherited QtQuick.Controls.AbstractButton.text property to define
 * the main text of the button.
 *
 * The trailing property (right-most side of the button) includes an arrow
 * pointing to the right by default and cannot be overridden.
 *
 * @since KirigamiAddons 0.11.0
 *
 * @inherit AbstractFormDelegate
 */
AbstractFormDelegate {
    id: root

    /**
     * @brief A label containing secondary text that appears under the
     * inherited text property.
     *
     * This provides additional information shown in a faint gray color.
     *
     * This is supposed to be a short text and the API user should avoid
     * making it longer than two lines.
     */
    property string description: ""

    /**
     * @brief This property allows to override the internal description
     * item (a QtQuick.Controls.Label) with a custom component.
     */
    property alias descriptionItem: internalDescriptionItem

    /**
     * @brief This property holds an item that will be displayed to the
     * left of the delegate's contents.
     *
     * default: `null`
     */
    property var leading: null

    /**
     * @brief This property holds the padding after the leading item.
     *
     * It is recommended to use Kirigami.Units here instead of direct values.
     *
     * @see Kirigami.Units
     */
    property real leadingPadding: Kirigami.Units.smallSpacing

    /** ADDED BY KRONE
     * @brief This property holds the direction of the arrow.
     *
     * @see FormCard.FormArrow
     */
    property int arrowDirection: FormArrow.Right

    focusPolicy: Qt.StrongFocus

    contentItem: RowLayout {
        spacing: 0

        Kirigami.Icon {
            visible: root.icon.name !== ""
            source: root.icon.name
            color: root.icon.color
            Layout.rightMargin: (root.icon.name !== "") ? Kirigami.Units.largeSpacing + Kirigami.Units.smallSpacing : 0
            implicitWidth: (root.icon.name !== "") ? Kirigami.Units.iconSizes.small : 0
            implicitHeight: (root.icon.name !== "") ? Kirigami.Units.iconSizes.small : 0
        }

        ColumnLayout {
            id: centralPart
            spacing: Kirigami.Units.smallSpacing
            // ADDED BY KRONE ===
            Layout.fillWidth: visible
            visible: (root.text !== "" || root.description !== "") // can't rely on children visibility
            // ===
            Label {
                Layout.fillWidth: true
                text: root.text
                elide: Text.ElideRight
                wrapMode: Text.Wrap
                maximumLineCount: 2
                color: root.enabled ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                Accessible.ignored: true // base class sets this text on root already
            }

            Label {
                id: internalDescriptionItem
                Layout.fillWidth: true
                text: root.description
                color: Kirigami.Theme.disabledTextColor
                elide: Text.ElideRight
                visible: root.description !== ""
                wrapMode: Text.Wrap
                Accessible.ignored: !visible
            }
        }

        FormArrow {
            Layout.alignment: (root.icon.name !== "" || centralPart.visible || root.leading) ? Qt.AlignRight | Qt.AlignVCenter : Qt.AlignHCenter | Qt.AlignVCenter
            direction: 0 <= root.arrowDirection < 4 ? root.arrowDirection : FormArrow.Right
        }
    }

    Accessible.onPressAction: action ? action.trigger() : root.clicked()
}

