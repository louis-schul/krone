// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami

import org.kde.krone.private 1.0

import "qrc:/contents/ui/components"

Kirigami.ApplicationWindow {
    id: root

    property string currentPage: "Main"
    property var currentItem
    readonly property bool isWideScreen: width >= Kirigami.Units.gridUnit * 60

    title: i18n("Krone")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 30

    onClosing: App.saveWindowGeometry(root)

    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()
    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()

    pageStack.popHiddenPages: true

    pageStack.columnView.columnResizeMode: Kirigami.ColumnView.SingleColumn
    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer

        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }

    globalDrawer: Sidebar {}

    Kirigami.PagePool { id: pagePool }

    function isMainPage() {
        return pageStack.currentItem == getPage("Main")
    }

    function getPage(name) {
        return pagePool.loadPage("qrc:/"+name+"Page.qml");
    }

    function switchToPage(pageName) {
        root.currentPage = pageName
        const page = getPage(pageName)

        pageStack.push(page);
        currentItem = pageStack.currentItem
    }

    Component.onCompleted: {
        App.restoreWindowGeometry(root)
        switchToPage("Main")
    }
}
