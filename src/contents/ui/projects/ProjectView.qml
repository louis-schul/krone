// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard
import org.kde.kirigamiaddons.components 1.0 as Components

import org.kde.krone.private 1.0

import "qrc:/contents/ui/components"

FormCard.FormCard {
    property alias deleteWarningDialog: cardsView.deleteWarningDialog

    Layout.alignment: Qt.AlignTop
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.topMargin: Kirigami.Units.largeSpacing

    FormCard.FormHeader {
        Layout.fillWidth: true
        title: i18n("Projects")
    }

    Kirigami.CardsListView {
        id: cardsView

        property var deleteWarningDialog

        clip: true
        model: DatabaseModels.projectsModel

        delegate: ProjectDelegate {}

        Layout.fillWidth: true
        Layout.fillHeight: true

        Components.FloatingButton {
            anchors {
                right: parent.right
                rightMargin: Kirigami.Units.largeSpacing
                bottom: parent.bottom
                bottomMargin: Kirigami.Units.largeSpacing
            }

            action: Kirigami.Action {
                text: "Add new item"

                icon.name: "list-add"

                onTriggered: {
                    projectDialog.amount = 0
                    projectDialog.projectName = ""
                    projectDialog.selectedColor = "#3daee9"
                    projectDialog.editing = false
                    projectDialog.open()
                }
            }
        }
    }
}

