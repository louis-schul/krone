// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.19 as Kirigami

import org.kde.krone.private 1.0

import "./colorDialog"

Kirigami.Dialog {
    property bool editing: false
    property int modelIndex: -1

    property alias groupName: nameField.text
    property alias selectedColor: picker.selectedColor

    title: i18n("Group Creator")
    padding: 0
    preferredWidth: Kirigami.Units.gridUnit * 16
    preferredHeight : Kirigami.Units.gridUnit * 21

    standardButtons: Kirigami.Dialog.Apply | Kirigami.Dialog.Cancel

    ColumnLayout {
        spacing: 0

        anchors.fill: parent

        ColorPicker{
            id:picker

            Layout.preferredWidth: Kirigami.Units.gridUnit * 16
            Layout.preferredHeight : Kirigami.Units.gridUnit * 12
        }

        Kirigami.Heading {
            text: i18n("Group name:")

            level: 4

            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.smallSpacing
            Layout.leftMargin: Kirigami.Units.largeSpacing
            Layout.rightMargin: Kirigami.Units.largeSpacing
        }

        Controls.TextField {
            id: nameField

            readOnly: text === i18nc("@defaultName:transactionGroup", "Default")

            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.smallSpacing
            Layout.leftMargin: Kirigami.Units.largeSpacing
            Layout.rightMargin: Kirigami.Units.largeSpacing
        }
    }

    onApplied: {
        if (groupName.length < 1) {
            nameField.placeholderText = i18n("This field can't be empty")
            nameField.forceActiveFocus()
            return
        }

        let message;
        let isSuccesFull;
        if (editing) {
            if (modelIndex === -1) {
                message = i18n("Error: Can't update this group, wrong ModelIndex")
                isSuccesFull = false
            } else {
                message = i18n("Error: Can't update this group, group name already exist")
                isSuccesFull = DatabaseModels.groupInfo.updateGroup(modelIndex, groupName, selectedColor)
            }
        } else {
            message = i18n("Error: Can't create this group, group name already exist")
            isSuccesFull = DatabaseModels.groupInfo.addGroup(groupName, selectedColor)
        }

        close()

        if (isSuccesFull) return

        applicationWindow().showPassiveNotification(message)
    }

    onClosed: {
        editing = false
        modelIndex = -1
    }
}


