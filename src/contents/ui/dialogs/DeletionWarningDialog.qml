// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import org.kde.kirigami 2.19 as Kirigami

Kirigami.PromptDialog {
    readonly property var useCaseTrad: {
        "transaction": i18nc("@deletion dialog, a money transaction", "transaction"),
        "group": i18nc("@deletion dialog, a money transaction group", "group"),
        "project": i18nc("@deletion dialog, a money savings project", "project")
    }

    property string useCase
    property int modelIndex: -1

    subtitle: i18nc("@label, %1 can be 'transaction' (a money transaction), 'group' (a money transaction group) or 'project' (a money savings 'project')","Are you sure you want to delete this %1 ?", useCaseTrad[useCase.toLowerCase()])
    standardButtons: Kirigami.Dialog.Ok | Kirigami.Dialog.Cancel

    onClosed: modelIndex = -1
}
