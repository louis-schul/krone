// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0

import org.kde.krone.private 1.0

import "./colorDialog"

Kirigami.Dialog {
    property bool editing: false
    property int modelIndex: -1

    property alias amount: amountField.value
    property alias projectName: projectField.text
    property alias selectedColor: picker.selectedColor

    title: i18n("Project Creator")
    padding: 0
    preferredWidth: Kirigami.Units.gridUnit * 16
    preferredHeight : Kirigami.Units.gridUnit * 24

    standardButtons: Kirigami.Dialog.Apply | Kirigami.Dialog.Cancel

    ColumnLayout {
        id: lay
        spacing: 0

        anchors.fill: parent

        ColorPicker{
            id: picker

            Layout.preferredWidth: Kirigami.Units.gridUnit * 16
            Layout.preferredHeight : Kirigami.Units.gridUnit * 10
        }

        FormTextFieldDelegate {
            id: projectField

            label: i18n("Project name:")
            status: Kirigami.MessageType.Error
            maximumLength: 40

            onFieldActiveFocusChanged: statusMessage = ""
        }

        FormTextFieldDelegate {
            id: amountField

            property real value
            property real max: 1000000000
            property real min: 0.00

            label: i18n("Goal amount:")
            status: Kirigami.MessageType.Error

            text: "0.00"
            validator: RegExpValidator {
                id: doubleChecker

                regExp: new RegExp("^[0-9]+(\\.[0-9]{1," + 2 + "})?$")
            }

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            onFieldActiveFocusChanged: statusMessage = ""
            onValueChanged: {
                if (value < 0) {
                    incomeExpenseDoubleButton.positive = false
                    value = value * -1
                }
                text = value
            }

            onTextChanged: {
                if (text.length === 0) {
                    text = "0.00"
                }

                value = parseFloat(text)
                if (value !== 0.00 && text.startsWith("0")) {
                    text = value
                } else if (value > max) {
                    text = max.toString()
                } else if (value < min) {
                    text = min.toString()
                }
            }
        }
    }

    onApplied: {
        if (projectName.trim().length < 1) {
            projectField.statusMessage = i18n("This field can't be empty")
            return
        }
        if (amount === 0.00) {
            amountField.statusMessage = i18n("Amount can't be 0")
            return
        }

        let message;
        let isSuccesFull;
        if (editing) {
            if (modelIndex === -1) {
                message = i18n("Error: Can't update this project, wrong ModelIndex")
                isSuccesFull = false
            } else {
                message = i18n("Error: Can't update this project, project name already exist")
                isSuccesFull = DatabaseModels.projectsModel.updateProject(modelIndex, projectName, amount, selectedColor)
            }
        } else {
            message = i18n("Error: Can't create this project, project name already exist")
            isSuccesFull = DatabaseModels.projectsModel.addProject(projectName, amount, selectedColor)
        }

        close()

        if (isSuccesFull) return

        applicationWindow().showPassiveNotification(message)
    }

    onClosed: {
        editing = false
        modelIndex = -1
    }
}



