// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.krone.private 1.0

import "qrc:/contents/ui/customAddons" as CustomAddons
Component {
    id: transactionDelegate
    Kirigami.AbstractCard {
        id: card

        topPadding: 0
        leftPadding: 0
        bottomPadding: 0
        rightPadding: 0
        implicitWidth: cardsView.width
        implicitHeight: Kirigami.Units.gridUnit * 8


        background: Kirigami.ShadowedRectangle {
            radius: 5
            color: Kirigami.ColorUtils.tintWithAlpha(
                Kirigami.Theme.backgroundColor,
                projectColor, 0.05)

            border {
                color: Kirigami.ColorUtils.tintWithAlpha(
                    Kirigami.Theme.backgroundColor,
                    Kirigami.Theme.textColor, 0.3)
                width: 1
            }

            shadow {
                size: 15
                xOffset: 5
                yOffset: 5
                color: Qt.rgba(0, 0, 0, 0.1)
            }
        }

        contentItem: Item {
            id: contItem

            RowLayout {
                id: itemLayout

                property int timing: 0

                anchors.fill: parent
                spacing: 0

                ColumnLayout {
                    id: mainHolder

                    spacing: Kirigami.Units.largeSpacing * 2

                    Layout.preferredHeight: optionsColumn.height
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop
                    Layout.preferredWidth: parent.width - optionButton.width

                    //Disable the animation when "loading" the page
                    onWidthChanged: if (itemLayout.timing === 0) {
                        const thresholdWidth = Kirigami.Units.gridUnit * 15

                        if (width >= thresholdWidth) itemLayout.timing = Kirigami.Units.shortDuration
                    }

                    states: State {
                        name: "invisible"
                        when: optionsColumn.isExtended
                        PropertyChanges { target: mainHolder; Layout.preferredWidth: 0 }
                        PropertyChanges { target: mainHolder; opacity: 0 }
                    }

                    Behavior on Layout.preferredWidth {
                        NumberAnimation { duration: itemLayout.timing }
                    }
                    Behavior on opacity {
                        NumberAnimation { duration: itemLayout.timing - 20}
                    }

                    Kirigami.Heading {
                        id: heading

                        text: projectName
                        level: 1
                        horizontalAlignment: Text.AlignHCenter

                        Layout.fillWidth: true
                    }

                    Item {
                        id: moneyProgressbar

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.margins: Kirigami.Units.gridUnit
                        Kirigami.ShadowedRectangle {
                            anchors.fill: parent
                            color: "#F34541"

                            radius: Kirigami.Units.smallSpacing

                            border {
                                width: 1
                                color: Kirigami.ColorUtils.tintWithAlpha(
                                    Kirigami.Theme.backgroundColor, Kirigami.Theme.textColor, 0.2)
                            }

                            shadow {
                                size: Kirigami.Units.gridUnit
                                color: Qt.rgba(0, 0, 0, 0.05)
                                yOffset: 2
                            }
                        }

                        Rectangle {
                            color: "#38B87C"
                            width: Math.round(parent.width * Math.min(percentage, 1))
                            height: parent.height
                            radius: Kirigami.Units.smallSpacing
                        }

                        Kirigami.Heading {
                            text: i18nc("@label %1 is current amount of money, %2 is goal amount of money", "%1/%2", currentAmount, goalAmount)
                            level: 1
                            horizontalAlignment: Text.AlignHCenter

                            anchors.fill: parent
                        }
                    }
                }

                ColumnLayout {
                    id: optionsColumn

                    property bool isExtended: false
                    property bool childVisible: width > (itemLayout.width) / 3

                    spacing: 0
                    opacity: 0
                    Layout.preferredWidth: 0
                    Layout.preferredHeight: Kirigami.Units.gridUnit * 8
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop

                    states: State {
                        name: "visible"
                        when: optionsColumn.isExtended
                        PropertyChanges { target: optionsColumn; Layout.preferredWidth: itemLayout.width - optionButton.width }
                        PropertyChanges { target: optionsColumn; opacity: 1 }
                    }

                    Behavior on Layout.preferredWidth {
                        NumberAnimation { duration: itemLayout.timing }
                    }
                    Behavior on opacity {
                        NumberAnimation { duration: itemLayout.timing - 20 }
                    }

                    FormCard.FormCheckDelegate {
                        id: check
                        text: i18n("Achieved")
                        visible:  optionsColumn.childVisible && model.achieved
                        Layout.fillHeight: true
                        Layout.preferredWidth: parent.width
                    }

                    FormCard.FormButtonDelegate {
                        id: editButton
                        icon.name: "edit-symbolic"
                        text: i18n("Edit")
                        visible: optionsColumn.childVisible
                        Layout.fillHeight: true
                        Layout.preferredWidth: parent.width
                    }

                    FormCard.FormButtonDelegate {
                        id: deleteButton
                        icon.name: "edit-delete-symbolic"
                        text: i18n("Delete")
                        visible: optionsColumn.childVisible
                        Layout.fillHeight: true
                        Layout.preferredWidth: parent.width

                        onClicked: {
                            cardsView.deleteWarningDialog.modelIndex = index

                            cardsView.deleteWarningDialog.open()
                        }
                    }
                }

                Kirigami.Separator {
                    Layout.fillHeight: true
                }

                CustomAddons.FormButtonDelegate {
                    id: optionButton

                    horizontalPadding: 0
                    arrowDirection: optionsColumn.isExtended
                        ? FormCard.FormArrow.Right
                        : FormCard.FormArrow.Left

                    Layout.margins: 0
                    Layout.fillHeight: true
                    Layout.preferredWidth: Kirigami.Units.gridUnit * 2 + Kirigami.Units.smallSpacing * 2
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop

                    onClicked: optionsColumn.isExtended = !optionsColumn.isExtended
                }
            }
        }
    }
}
