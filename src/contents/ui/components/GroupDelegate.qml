// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.krone.private 1.0

import "qrc:/contents/ui/customAddons" as CustomAddons

Component {
    id: groupDelegate

    Kirigami.AbstractCard {
        id: card

        topPadding: 0
        leftPadding: 0
        bottomPadding: 0
        rightPadding: 0

        implicitWidth: cardsView.width
        implicitHeight: Kirigami.Units.gridUnit * 5

        background: Kirigami.ShadowedRectangle {
            color: Kirigami.Theme.backgroundColor
            radius: Kirigami.Units.smallSpacing

            border {
                width: 1
                color: Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, Kirigami.Theme.textColor, 0.2)
            }

            shadow {
                size: Kirigami.Units.gridUnit
                color: Qt.rgba(0, 0, 0, 0.05)
                yOffset: 2
            }
        }

        contentItem: Item {
            RowLayout {
                id: itemLayout

                property int generalTiming: 0

                anchors.fill: parent
                spacing: 0

                RowLayout {
                    id: mainHolder
                    Layout.alignment: Qt.AlignRight
                    Layout.margins: 0
                    Layout.fillHeight: true
                    // fillWidth will make optionButton not respect its preferredWidth
                    Layout.preferredWidth: parent.width - optionButton.width

                    //Disable the animation when "loading" the page
                    onWidthChanged: if (itemLayout.generalTiming === 0) {
                        const thresholdWidth = cardsView.width * 0.7

                        if (width >= thresholdWidth && thresholdWidth !== 0) itemLayout.generalTiming = Kirigami.Units.shortDuration
                    }

                    states: State {
                        name: "invisible"
                        when: optionsColumn.isExpended
                        PropertyChanges { target: mainHolder; Layout.preferredWidth: 0 }
                        PropertyChanges { target: mainHolder; opacity: 0 }
                    }

                    Behavior on Layout.preferredWidth {
                        NumberAnimation { duration: itemLayout.generalTiming }
                    }
                    Behavior on opacity {
                        NumberAnimation { duration: itemLayout.generalTiming - 20}
                    }

                    spacing: Kirigami.Units.largeSpacing

                    Kirigami.Heading {
                        id: heading

                        text: name
                        level: 1

                        Layout.fillHeight: true
                        Layout.preferredWidth: (cardsView.width - Kirigami.Units.gridUnit * 5) / 2
                        Layout.margins: Kirigami.Units.largeSpacing
                    }

                    Rectangle {
                        id: colorButton

                        color: model.color
                        radius: Kirigami.Units.smallSpacing

                        Layout.fillWidth: true
                        Layout.preferredWidth: heading.width
                        Layout.preferredHeight: Kirigami.Units.gridUnit * 2
                        Layout.margins: Kirigami.Units.largeSpacing
                    }
                }

                ColumnLayout {
                    id: optionsColumn

                    property bool isExpended: false
                    property bool childVisible: width > (itemLayout.width) / 3

                    spacing: 0
                    opacity: 0
                    Layout.preferredWidth: 0
                    Layout.preferredHeight: Kirigami.Units.gridUnit * 8
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop

                    states: State {
                        name: "visible"
                        when: optionsColumn.isExpended
                        PropertyChanges { target: optionsColumn; Layout.preferredWidth: itemLayout.width - optionButton.width }
                        PropertyChanges { target: optionsColumn; opacity: 1 }
                    }

                    Behavior on Layout.preferredWidth {
                        NumberAnimation { duration: itemLayout.generalTiming }
                    }
                    Behavior on opacity {
                        NumberAnimation { duration: itemLayout.generalTiming - 20 }
                    }

                    FormCard.FormButtonDelegate {
                        id: editButton
                        icon.name: "edit-symbolic"
                        text: i18n("Edit")
                        visible: optionsColumn.childVisible
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        onClicked: {

                        }
                    }

                    FormCard.FormButtonDelegate {
                        id: deleteButton
                        icon.name: "edit-delete-symbolic"
                        text: i18n("Delete")
                        visible: optionsColumn.childVisible && model.name !== i18nc("@defaultName:transactionGroup", "Default")
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        onClicked: {
                            cardsView.deleteWarningDialog.modelIndex = index

                            cardsView.deleteWarningDialog.open()
                        }
                    }
                }

                Kirigami.Separator {
                    id: optionSeperator

                    Layout.fillHeight: true
                }

                CustomAddons.FormButtonDelegate {
                    id: optionButton

                    horizontalPadding: 0
                    arrowDirection: optionsColumn.isExpended
                        ? FormCard.FormArrow.Right
                        : FormCard.FormArrow.Left

                    Layout.fillHeight: true
                    Layout.preferredWidth: Kirigami.Units.gridUnit * 2 + Kirigami.Units.largeSpacing

                    onClicked: optionsColumn.isExpended = !optionsColumn.isExpended
                }
            }
        }
    }
}

