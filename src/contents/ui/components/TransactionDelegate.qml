// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.krone.private 1.0

import "qrc:/contents/ui/customAddons" as CustomAddons

Component {
    id: transactionDelegate

    Kirigami.AbstractCard {
        id: card

        topPadding: 0
        leftPadding: 0
        bottomPadding: 0
        rightPadding: 0

        implicitWidth: cardsView.width
        implicitHeight: mainHolder.height + Kirigami.Units.largeSpacing

        background: Kirigami.ShadowedRectangle {
            color: Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, groupColor,  0.05)

            radius: Kirigami.Units.smallSpacing

            border {
                width: 1
                color: Kirigami.ColorUtils.tintWithAlpha(
                    color, Kirigami.Theme.textColor, 0.2)
            }

            shadow {
                size: Kirigami.Units.gridUnit
                color: Qt.rgba(0, 0, 0, 0.05)
                yOffset: 2
            }
        }

        contentItem: Item {
            RowLayout {
                id: itemLayout

                property int generalTiming: 0
                property bool descWasExpended: false
                property int descriptionTiming: Kirigami.Units.shortDuration

                anchors.fill: parent
                spacing: 0

                GridLayout {
                    id: mainHolder
                    Layout.alignment: Qt.AlignRight
                    Layout.margins: 0
                    Layout.fillHeight: true
                    // fillWidth will make optionButton not respect its preferredWidth
                    Layout.preferredWidth: parent.width - optionButton.width

                    //Disable the animation when "loading" the page
                    onWidthChanged: if (itemLayout.generalTiming === 0) {
                        const thresholdWidth = cardsView.width * 0.7

                        if (width >= thresholdWidth && thresholdWidth !== 0) itemLayout.generalTiming = Kirigami.Units.shortDuration
                    }

                    states: State {
                        name: "invisible"
                        when: optionsColumn.isExpended
                        PropertyChanges { target: mainHolder; Layout.preferredWidth: 0 }
                        PropertyChanges { target: mainHolder; opacity: 0 }
                    }

                    Behavior on Layout.preferredWidth {
                        NumberAnimation { duration: itemLayout.generalTiming }
                    }
                    Behavior on opacity {
                        NumberAnimation { duration: itemLayout.generalTiming - 20}
                    }

                    rows: 4
                    columns: 3
                    columnSpacing: Kirigami.Units.smallSpacing
                    rowSpacing: Kirigami.Units.smallSpacing

                    Kirigami.Heading {
                        id: amountLabel

                        Layout.row: 0
                        Layout.column: 0

                        Layout.preferredWidth: Kirigami.Units.gridUnit * 5
                        Layout.topMargin: Kirigami.Units.gridUnit
                        Layout.leftMargin: Kirigami.Units.largeSpacing
                        Layout.alignment: Qt.AlignTop

                        text:{
                            // Prevent weird convertion error between C++ and qml
                            let actualAmount = amount/100
                            if (actualAmount / 1000000 >= 1) {
                                actualAmount = (actualAmount / 1000000).toFixed(2) + "M"
                            } else if (actualAmount / 1000 >= 1) {
                                actualAmount = (actualAmount / 1000).toFixed(2) + "K"
                            }

                            return actualAmount + " " + model.currency
                        }
                        level: 1
                        elide: Text.ElideMiddle
                        color: amount > 0 ? "#38B87C" : "#F34541"
                    }

                    Kirigami.Heading {
                        id: displayTitle

                        Layout.row: 0
                        Layout.column: 1
                        Layout.fillWidth: true

                        text: title
                        level: 1
                        elide: Text.ElideRight
                    }

                    Kirigami.Heading {
                        id: displayDate

                        Layout.row: 0
                        Layout.column: 2
                        Layout.fillWidth: true;
                        Layout.rightMargin: Kirigami.Units.smallSpacing

                        text: {
                            const dbDate = date

                            // date is stored "YEAR-MONTH-DAY" in SQL we want DAY/MONTH/YEAR
                            // TODO: make it MONTH/DAY/YEAR when needed
                            const info = date.split("-")
                            return info[2] + "/" + info[1] + "/" + info[0]
                        }
                        level: 6
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignRight
                    }

                    Kirigami.Separator {
                        id: centralSeperator

                        Layout.row: 1
                        Layout.column: 1
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        Layout.rightMargin: Kirigami.Units.smallSpacing
                    }

                    Controls.Label {
                        id: descriptionLabel

                        Layout.row: 2
                        Layout.column: 1

                        Layout.rowSpan: 1
                        Layout.columnSpan: 2

                        Layout.fillWidth: true

                        text: description
                        elide: Text.ElideRight
                        wrapMode: Text.WrapAnywhere
                        maximumLineCount: 1

                        Behavior on maximumLineCount {
                            NumberAnimation { duration: itemLayout.descriptionTiming }
                        }
                    }

                    Item {
                        id: expendPlaceHolder

                        Layout.fillWidth: true
                        Layout.preferredHeight: visible
                            ? Kirigami.Units.iconSizes.small * 2
                            : 0
                        Layout.bottomMargin: - Kirigami.Units.smallSpacing

                        visible: !expendDescButton.visible
                    }

                    CustomAddons.FormButtonDelegate {
                        id: expendDescButton

                        Layout.row: 3
                        Layout.column: 0

                        Layout.columnSpan: 3

                        Layout.fillWidth: true
                        Layout.preferredHeight: visible
                            ? Kirigami.Units.iconSizes.small * 2
                            : 0
                        Layout.bottomMargin: - Kirigami.Units.smallSpacing

                        arrowDirection: descriptionLabel.truncated
                        ? FormCard.FormArrow.Down
                        : FormCard.FormArrow.Up

                        visible: descriptionLabel.truncated || descriptionLabel.maximumLineCount > 1

                        onClicked: descriptionLabel.maximumLineCount = descriptionLabel.truncated
                            ? 12
                            : 1
                    }
                }

                ColumnLayout {
                    id: optionsColumn

                    property bool isExpended: false
                    property bool childVisible: width > (itemLayout.width) / 3

                    spacing: 0
                    opacity: 0
                    Layout.preferredWidth: 0
                    Layout.preferredHeight: Kirigami.Units.gridUnit * 8
                    Layout.alignment: Qt.AlignRight | Qt.AlignTop

                    states: State {
                        name: "visible"
                        when: optionsColumn.isExpended
                        PropertyChanges { target: optionsColumn; Layout.preferredWidth: itemLayout.width - optionButton.width }
                        PropertyChanges { target: optionsColumn; opacity: 1 }
                    }

                    Behavior on Layout.preferredWidth {
                        NumberAnimation { duration: itemLayout.generalTiming }
                    }
                    Behavior on opacity {
                        NumberAnimation { duration: itemLayout.generalTiming - 20 }
                    }

                    FormCard.FormButtonDelegate {
                        id: editButton
                        icon.name: "edit-symbolic"
                        text: i18n("Edit")
                        visible: optionsColumn.childVisible
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        onClicked: {
                            applicationWindow().switchToPage("TransactionCreation")

                            const transactionPage = applicationWindow().pageStack.currentItem

                            transactionPage.amount = amount/100
                            transactionPage.titleText = title
                            transactionPage.description = description

                            let dateObject = new Date(date)
                            transactionPage.date = dateObject

                            transactionPage.group = group
                            transactionPage.editing = true
                            transactionPage.modelIndex = model.index
                        }
                    }

                    FormCard.FormButtonDelegate {
                        id: deleteButton
                        icon.name: "edit-delete-symbolic"
                        text: i18n("Delete")
                        visible: optionsColumn.childVisible
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        onClicked: {
                            cardsView.deleteWarningDialog.modelIndex = index

                            cardsView.deleteWarningDialog.open()
                        }
                    }
                }

                Kirigami.Separator {
                    id: optionSeperator

                    Layout.fillHeight: true
                }

                CustomAddons.FormButtonDelegate {
                    id: optionButton

                    horizontalPadding: 0
                    arrowDirection: optionsColumn.isExpended
                        ? FormCard.FormArrow.Right
                        : FormCard.FormArrow.Left

                    Layout.fillHeight: true
                    Layout.preferredWidth: Kirigami.Units.gridUnit * 2 + Kirigami.Units.largeSpacing

                    onClicked: {
                        if (!optionsColumn.isExpended && descriptionLabel.maximumLineCount > 1) {
                            expendDescButton.clicked()
                            itemLayout.descWasExpended = true
                        } else if (itemLayout.descWasExpended) {
                            // Make the transition smoother
                            itemLayout.descriptionTiming = Kirigami.Units.shortDuration * 2
                            expendDescButton.clicked()
                            itemLayout.descWasExpended = false
                            itemLayout.descriptionTiming = Kirigami.Units.shortDuration
                        }

                        optionsColumn.isExpended = !optionsColumn.isExpended
                    }
                }
            }
        }
    }
}
