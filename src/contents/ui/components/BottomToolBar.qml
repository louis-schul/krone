// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2022 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.1
import org.kde.kirigami 2.19 as Kirigami

Kirigami.NavigationTabBar {
    id: tool

    recolorIcons: false
    currentIndex: 0

    actions: [
        Kirigami.Action {
            id: overviewAction
            property var isWideScreen: applicationWindow().isWideScreen

            onIsWideScreenChanged: if (isWideScreen) {
                overviewAction.triggered()
                overviewAction.checked = true
            }

            text: i18n("Overview")
            icon.name: "view-financial-account-cash"

            onTriggered: applicationWindow().pageStack.currentItem.currentView = "overview"
        },
        Kirigami.Action {
            text: i18n("Projects")
            icon.name: "view-financial-account-loan"

            onTriggered: applicationWindow().pageStack.currentItem.currentView = "projects"
        }
    ]
}

