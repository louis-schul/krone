// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.5 as Kirigami

Kirigami.OverlayDrawer {
    id: drawer

    edge: Qt.application.layoutDirection == Qt.RightToLeft ? Qt.RightEdge : Qt.LeftEdge
    handleOpenIcon.source: null
    handleClosedIcon.source: null
    handleVisible: applicationWindow().isMainPage()

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    width: Kirigami.Units.gridUnit * 15
    contentItem: ColumnLayout {
        id: column

        implicitWidth: Kirigami.Units.gridUnit * 15
        width: Kirigami.Units.gridUnit * 15

        Item { Layout.fillWidth: true; Layout.fillHeight: true }

        Kirigami.Separator {
            Layout.fillWidth: true
            Layout.alignment:Qt.AlignBottom
        }

        Kirigami.BasicListItem {
            text: i18n("Settings")
            icon: "settings-configure"

            Layout.alignment: Qt.AlignBottom

            onClicked: {
                applicationWindow().switchToPage('Settings')
                drawer.close()
            }
        }

        Kirigami.BasicListItem {
            text: i18n("About Krone")
            icon: "help-about"

            onClicked: {
                applicationWindow().switchToPage('About')
                drawer.close()
            }
        }
    }
}


