// SPDX-License-Identifier: GPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

import org.kde.krone.private 1.0

MobileForm.AboutPage {
    aboutData: About
}
