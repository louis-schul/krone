// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.19 as Kirigami

import org.kde.krone.private 1.0

import "qrc:/contents/ui/accountOverview"
import "qrc:/contents/ui/components"
import "qrc:/contents/ui/dialogs"
import "qrc:/contents/ui/projects"

// ScrollablePage has a weird bug on resize, the combo Page+Scrollview work better here
Kirigami.Page {
    id: root

    property string currentView: "overview"

    title: i18nc("@title:window", "Account overview")

    leftPadding: applicationWindow().isWideScreen ? Kirigami.Units.gridUnit : 0
    rightPadding: applicationWindow().isWideScreen ? Kirigami.Units.gridUnit : 0
    topPadding: Kirigami.Units.gridUnit
    bottomPadding: Kirigami.Units.gridUnit

    data: [
        ProjectDialog {
            id: projectDialog
        },
        DeletionWarningDialog {
            id: projectDeleteWarningDialog

            useCase: "project"

            // NOTE: Change this when introducing KSortFilterProxyModel for "achieved" or not
            onAccepted: DatabaseModels.projectsModel.removeProject(modelIndex)
        },
        DeletionWarningDialog {
            id: transactionDeleteWarningDialog

            useCase: "transaction"

            onAccepted: {
                const qModelIndex = searchFilterProxyModel.mapToSource(searchFilterProxyModel.index(modelIndex, 0))
                DatabaseModels.accountModel.removeTransaction(qModelIndex.row)
            }
        }
    ]

    Loader {
        id: editorLoader

        anchors.fill: parent
        active: root.currentView === "overview"

        sourceComponent: Controls.ScrollView {
            id: scrollView
            anchors.fill: parent
            Overview {
                anchors.fill: parent
                availableWidth: scrollView.availableWidth
                availableHeight: scrollView.availableHeight
                deleteWarningDialog: transactionDeleteWarningDialog
            }
        }
    }

    Loader {
        id: projectLoader

        sourceComponent: ProjectView {
            anchors.fill: parent
            deleteWarningDialog: projectDeleteWarningDialog
        }
        active: !editorLoader.active
        anchors.fill: parent
    }

    footer: BottomToolBar {
        id: bottomToolBar
        visible: !applicationWindow().isWideScreen
    }
}
