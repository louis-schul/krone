// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.dateandtime 0.1 as DateAndTime
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

import org.kde.krone.private 1.0

import "qrc:/contents/ui/dialogs"
import "qrc:/contents/ui/components"
import "qrc:/contents/ui/customAddons" as CustomAddons

FormCard.FormCardPage {
    id: root

    property int modelIndex
    property bool editing: false
    property date date: new Date()

    property alias group: groupField.group
    property alias amount: amountField.value
    property alias dateText: dateField.text
    property alias titleText: titleField.text
    property alias description: descriptionField.text

    title: editing ? i18nc("@title:window", "Edit transaction") : i18nc("@title:window", "Add transaction")

    leftPadding: 0
    rightPadding: 0
    topPadding: Kirigami.Units.gridUnit
    bottomPadding: Kirigami.Units.gridUnit

    onDateChanged: {
        picker.clickedDate = date
        picker.selectedDate = date
    }

    onBackRequested: clean()

    FormCard.FormCard {
        RowLayout {
            Layout.fillWidth: true
            Layout.preferredHeight: amountField.height
            spacing: 0

            FormCard.FormTextFieldDelegate {
                id: amountField

                property real value
                property real max: 1000000000
                property real min: 0.00

                label: i18n("Amount:")
                status: Kirigami.MessageType.Error

                text: "0.00"
                validator: RegExpValidator {
                    id: doubleChecker

                    regExp: new RegExp("^[0-9]+(\\.[0-9]{1," + 2 + "})?$")
                }

                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop

                onFieldActiveFocusChanged: statusMessage = ""
                onValueChanged: {
                    if (value < 0) {
                        incomeExpenseDoubleButton.positive = false
                        value = value * -1
                    }
                    text = value
                }

                onTextChanged: {
                    if (text.length === 0) {
                        text = "0.00"
                    }

                    value = parseFloat(text)
                    if (value !== 0.00 && text.startsWith("0")) {
                        text = value
                    } else if (value > max) {
                        text = max.toString()
                    } else if (value < min) {
                        text = min.toString()
                    }
                }
            }

            CustomAddons.IncomeExpenseDoubleButton {
                id: incomeExpenseDoubleButton

                leftIcon: "list-add-symbolic"
                rightIcon: "list-remove-symbolic"

                Layout.fillHeight: true
                Layout.preferredWidth: Kirigami.Units.gridUnit * 7
                Layout.alignment: Qt.AlignTop
            }
        }
    }

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing

        FormCard.FormTextFieldDelegate {
            id: titleField

            text: ""
            label: i18n("Title:")
            status: Kirigami.MessageType.Error

            onFieldActiveFocusChanged: statusMessage = ""

            maximumLength: 50
        }
    }

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing

        RowLayout {
            FormCard.FormTextDelegate {
                text: i18n("Description:")

                Layout.alignment: Qt.AlignBottom
            }

            Controls.Label {
                TextMetrics {
                    id: metrics
                    text: label(240)
                    font: Kirigami.Theme.smallFont

                    function label(current: int): string {
                        return i18nc("@label %1 is current text length, %2 is maximum length of text field", "%1/%2", current, 240)
                    }
                }
                text: metrics.label(descriptionField.text.length)
                font: Kirigami.Theme.smallFont
                color: descriptionField.text.length === 240
                    ? Kirigami.Theme.neutralTextColor
                    : Kirigami.Theme.textColor
                horizontalAlignment: Text.AlignRight

                Layout.margins: Kirigami.Units.smallSpacing
                Layout.preferredWidth: metrics.advanceWidth
            }
            Layout.fillWidth: true
            Layout.leftMargin: Kirigami.Units.smallSpacing
            Layout.rightMargin: Kirigami.Units.largeSpacing * 2 + Kirigami.Units.smallSpacing
            Layout.bottomMargin: - Kirigami.Units.smallSpacing
        }

        Controls.ScrollView {
            Layout.fillWidth: true
            Layout.preferredHeight: Kirigami.Units.gridUnit * 6

            Layout.leftMargin: Kirigami.Units.largeSpacing * 2  + Kirigami.Units.smallSpacing
            Layout.rightMargin: Kirigami.Units.largeSpacing * 2  + Kirigami.Units.smallSpacing
            Layout.bottomMargin: Kirigami.Units.largeSpacing + Kirigami.Units.smallSpacing

            Controls.TextArea {
                id: descriptionField

                wrapMode: TextEdit.Wrap

                onTextChanged: if (text.length > 240) remove(cursorPosition-1, cursorPosition)
            }
        }
    }

    FormCard.FormCard {
        Layout.fillWidth: true

        Layout.topMargin: Kirigami.Units.largeSpacing

        RowLayout {
            Layout.fillWidth: true
            spacing: 0
            FormCard.FormTextFieldDelegate {
                id: dateField

                text: picker.formatedDate
                label: i18n("Date:")

                readOnly: true
            }

            CustomAddons.FormButtonDelegate {
                Layout.margins: 0
                Layout.fillHeight: true
                Layout.preferredWidth: Kirigami.Units.gridUnit * 2 + Kirigami.Units.smallSpacing * 2

                arrowDirection: picker.showPicker ? FormCard.FormArrow.Up : FormCard.FormArrow.Down

                onClicked: picker.showPicker = !picker.showPicker
            }
        }

        DateAndTime.DatePicker {
            id: picker

            property bool showPicker: false

            Layout.fillWidth: true
            Layout.preferredHeight: Kirigami.Units.gridUnit * 20

            visible: Layout.preferredHeight !== 0

            states: State {
                name: "invisible"
                when: !picker.showPicker
                PropertyChanges { target: picker; Layout.preferredHeight: 0 }
            }

            Behavior on Layout.preferredHeight {
                NumberAnimation { duration: Kirigami.Units.shortDuration }
            }

            onSelectedDateChanged: {
                // Using date, month and year property doesn't work
                // this would result in the text being one pick behind
                dateField.text = selectedDate.getDate() + "/" + (selectedDate.getMonth()+1) + "/" + selectedDate.getFullYear()
            }
        }
    }

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing

        RowLayout {
            Layout.fillWidth: true
            spacing: 0

            FormCard.FormComboBoxDelegate {
                id: groupField

                property string group: i18nc("@defaultName:transactionGroup", "Default")
                property bool initializing: false

                text: i18n("Group:")
                model: DatabaseModels.groupInfo
                textRole: "name"
                displayMode: FormCard.FormComboBoxDelegate.Dialog

                onCurrentTextChanged: if (root.visible && !initializing) group = currentText

                onGroupChanged: if (group !== currentText) {
                    initializing = true
                    for (let i = 0; i < model.length ; i++) {
                        currentIndex = i;

                        if (currentText === group) {
                            initializing = false
                            return
                        }
                    }
                    currentIndex = 0;
                    initializing = false
                }
            }

            CustomAddons.FormButtonDelegate {
                icon.name: "edit-symbolic"

                Layout.fillHeight: true
                Layout.preferredWidth: Kirigami.Units.gridUnit * 2 + Kirigami.Units.smallSpacing * 2

                onClicked: applicationWindow().switchToPage("Settings")
            }
        }
    }

    footer: RowLayout {
        spacing: 0

        FormCard.FormButtonDelegate {
            id: saveButton

            icon.name: "document-save-symbolic"

            text: i18n("Save")
            visible: root.editing

            background: FormCard.FormDelegateBackground {
                control: saveButton
                color: {
                    const greenColor = "#38B87C"
                    let colorOpacity = 0;

                    if (!control.enabled) {
                        colorOpacity = 0.7;
                    } else if (control.pressed) {
                        colorOpacity = 0.9;
                    } else if (control.visualFocus) {
                        colorOpacity = 0.8;
                    } else if (!Kirigami.Settings.tabletMode && control.hovered) {
                        colorOpacity = 0.77;
                    }

                    return Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, greenColor, colorOpacity)
                }
            }


            Layout.preferredWidth: visible ? parent.width / 2 : 0

            onClicked: root.saveOrApplyTransaction()
        }

        FormCard.FormButtonDelegate {
            id: deleteButton

            icon.name: "user-trash-symbolic"

            text: i18n("Delete")
            visible: root.editing

            background: FormCard.FormDelegateBackground {
                control: deleteButton
                color: {
                    const redColor = "#F34541"
                    let colorOpacity = 0;

                    if (!control.enabled) {
                        colorOpacity = 0.7;
                    } else if (control.pressed) {
                        colorOpacity = 0.9;
                    } else if (control.visualFocus) {
                        colorOpacity = 0.8;
                    } else if (!Kirigami.Settings.tabletMode && control.hovered) {
                        colorOpacity = 0.77;
                    }

                    return Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, redColor, colorOpacity)
                }
            }

            Layout.preferredWidth: visible ? parent.width / 2 : 0

            onClicked: {
                DatabaseModels.accountModel.removeTransaction(root.modelIndex)
                clean()
            }
        }

        FormCard.FormButtonDelegate {
            id: applyButton

            icon.name: "dialog-apply"

            text: i18n("Apply")
            visible: !root.editing

            background: FormCard.FormDelegateBackground {
                control: applyButton
                color: {
                    const greenColor = "#38B87C"
                    let colorOpacity = 0;

                    if (!control.enabled) {
                        colorOpacity = 0.7;
                    } else if (control.pressed) {
                        colorOpacity = 0.9;
                    } else if (control.visualFocus) {
                        colorOpacity = 0.8;
                    } else if (!Kirigami.Settings.tabletMode && control.hovered) {
                        colorOpacity = 0.77;
                    }

                    return Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, greenColor, colorOpacity)
                }
            }

            Layout.preferredWidth: visible ? parent.width : 0

            onClicked: root.saveOrApplyTransaction()
        }
    }

    function saveOrApplyTransaction() {
        if (amount === 0.00) {
            amountField.statusMessage = i18n("Amount can't be 0")
            return
        }
        if (titleText.trim().length === 0) {
            titleField.statusMessage = i18n("Title can't be blank")
            return
        }

        const multiplicator = incomeExpenseDoubleButton.positive ? 1 : -1
        const finalAmount = parseInt(amount*100) * multiplicator

        if (!editing) {
            // pass the amount as an int and convert it back to a float from C++ to prevent weird convertion
            DatabaseModels.accountModel.addTransaction(finalAmount, titleText.trim(), description.trim(), dateText, group, "EUR")
        } else {
            DatabaseModels.accountModel.updateTransaction(modelIndex, finalAmount, titleText.trim(), description.trim(), dateText, group, "EUR")
        }
        clean()
    }

    function clean() {
        amount = 0
        amountField.text = "0.00"
        amountField.statusMessage = ""
        titleText = ""
        titleField.statusMessage = ""
        description = ""
        date = new Date()
        editing = false
        modelIndex = -1

        picker.showPicker = false

        incomeExpenseDoubleButton.positive = true
    }
}

