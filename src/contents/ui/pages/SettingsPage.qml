// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard
import org.kde.kirigamiaddons.components 1.0 as Components

import org.kde.krone.private 1.0

import "qrc:/contents/ui/dialogs"
import "qrc:/contents/ui/components"
import "qrc:/contents/ui/customAddons" as CustomAddons

FormCard.FormCardPage {
    id: root

    title: i18nc("@title:window", "Settings")

    leftPadding: 0
    rightPadding: 0
    topPadding: Kirigami.Units.gridUnit
    bottomPadding: Kirigami.Units.gridUnit

    data: [
        GroupDialog {
            id: groupDialog
        },
        DeletionWarningDialog {
            id: deleteWarningDialog

            useCase: "group"

            onAccepted: DatabaseModels.groupInfo.removeGroup(modelIndex)
        }
    ]

    // TODO: link that to config
    FormCard.FormCard {
        FormCard.FormComboBoxDelegate {
            id: currencyBox

            text: i18n("Default currency")+":"
            model: DatabaseModels.accountModel.currencies

            displayMode: FormCard.FormComboBoxDelegate.Dialog
            currentIndex: 0
        }
    }

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing

        FormCard.FormHeader {
            Layout.fillWidth: true
            title: i18n("Transaction Group")
        }

        Kirigami.CardsListView {
            id: cardsView

            readonly property var deleteWarningDialog: deleteWarningDialog

            clip: true
            model: DatabaseModels.groupInfo

            delegate: GroupDelegate {}
/*
            Kirigami.AbstractCard {
                topPadding: 0
                leftPadding: 0
                bottomPadding: 0
                rightPadding: 0
                implicitWidth: cardsView.width
                implicitHeight: Kirigami.Units.gridUnit * 3

                contentItem: Item {
                    id: contItem

                    MouseArea {
                        width: {
                            const buttonWidth = deleteButton.visible ? deleteButton.width : 0

                            return contItem.width - buttonWidth - Kirigami.Units.largeSpacing
                        }

                        anchors {
                            top: contItem.top
                            bottom: contItem.bottom
                            left: contItem.left
                        }

                        onClicked: {
                            groupDialog.groupName = model.name
                            groupDialog.selectedColor = model.color
                            groupDialog.editing = true
                            groupDialog.modelIndex = model.index
                            groupDialog.open()
                        }
                    }

                    RowLayout {
                        anchors.fill: parent
                        spacing: Kirigami.Units.largeSpacing

                        Kirigami.Heading {
                            id: heading

                            text: model.name
                            level: 1

                            Layout.fillHeight: true
                            Layout.preferredWidth: (cardsView.width - Kirigami.Units.gridUnit * 5) / 2
                            Layout.margins: Kirigami.Units.largeSpacing
                        }

                        Rectangle {
                            id: colorButton

                            color: model.color
                            radius: Kirigami.Units.smallSpacing

                            Layout.fillWidth: true
                            Layout.preferredWidth: heading.width
                            Layout.preferredHeight: Kirigami.Units.gridUnit * 2
                            Layout.margins: Kirigami.Units.largeSpacing
                        }

                        CustomAddons.FormButtonDelegate {
                            id: deleteButton

                            visible: model.name !== i18nc("@defaultName:transactionGroup", "Default")
                            icon.name: "edit-delete-symbolic"
                            Layout.fillHeight: true
                            Layout.margins: 0
                            Layout.preferredWidth: Kirigami.Units.gridUnit * 4

                            onClicked: console.log(model.index)//DatabaseModels.groupInfo.removeGroup(model.index)
                        }
                    }
                }
            }*/

            Layout.fillWidth: true
            Layout.preferredHeight: Kirigami.Units.gridUnit * 20 - Kirigami.Units.largeSpacing

            Components.FloatingButton {
                anchors {
                    right: parent.right
                    rightMargin: Kirigami.Units.largeSpacing
                    bottom: parent.bottom
                    bottomMargin: Kirigami.Units.largeSpacing
                }

                action: Kirigami.Action {
                    text: "Add new item"

                    icon.name: "list-add"

                    onTriggered: {
                        groupDialog.groupName = ""
                        groupDialog.selectedColor = "#3daee9"
                        groupDialog.editing = false
                        groupDialog.open()
                    }
                }
            }
        }
    }
}
