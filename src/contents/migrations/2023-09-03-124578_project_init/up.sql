/*
SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
SPDX-License-Identifier: GPL-3.0-or-later
*/

CREATE TABLE IF NOT EXISTS Projects (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    NAME TEXT NOT NULL,
    GoalAmount INTEGER NOT NULL,
    CurrentAmount INTEGER DEFAULT 0,
    Achieved bool DEFAULT false,
    Color BLOB NOT NULL
)
