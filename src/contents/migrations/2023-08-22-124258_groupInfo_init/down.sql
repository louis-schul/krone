/*
SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
SPDX-License-Identifier: GPL-3.0-or-later
*/

DROP TABLE GroupInfo;
