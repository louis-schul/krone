/*
SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
SPDX-License-Identifier: GPL-3.0-or-later
*/

CREATE TABLE IF NOT EXISTS GroupInfo (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    NAME TEXT NOT NULL,
    Color BLOB NOT NULL
)
