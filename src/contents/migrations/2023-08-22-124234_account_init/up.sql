/*
SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
SPDX-License-Identifier: GPL-3.0-or-later
*/

CREATE TABLE IF NOT EXISTS Account (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Amount INTEGER NOT NULL,
    Title TEXT NOT NULL,
    Description TEXT,
    Date DATE NOT NULL,
    GroupName TEXT NOT NULL,
    Currency TEXT NOT NULL,
    ProjectID int DEFAULT -1
)
