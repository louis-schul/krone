/*
SPDX-FileCopyrightText: 2023 Louis Schul schul9louis@gmail.com
SPDX-License-Identifier: GPL-3.0-or-later
*/

CREATE TABLE IF NOT EXISTS Total (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Earned INT DEFAULT 0,
    Lost INT DEFAULT 0
)
